# **Theory: Caesar Cipher: Encryption**

##
# Cryptography Algorithms:
Cryptographic algorithms are used to ___transfer electronic data over the internet so that no third-party is able to read the data.___ The strength of the code is judged according to four parameters confidentiality, Integrity, Non – repudiation and authentication.

![blockdaigram](blockdaigram.png)

## Caesar Cipher Cryptography:
The Caesar Cipher, also known as a **shift cipher**, is one of the oldest and simplest forms of encrypting a message. It is a type of substitution cipher where each letter in the original message (which in cryptography is called the plaintext) is replaced with a letter corresponding to a certain number of letters shifted up or down in the alphabet. As shown in fig1 given below for each letter of the alphabet, you would take its position in the alphabet, say 3 for the letter 'A', and shift it by the key number. If we had a key of +3, that 'A' would be shifted down to an 'D' - and that same process would be applied to every letter in the plaintext. In this way, a message that initially was quite readable, ends up in a form that cannot be understood at a simple glance.

![shift cipher](shift.jpg)

**The Caesar Cipher can be expressed in a more mathematical form as follows:** In plain terms, this means that the encryption of a letter x is equal to a **shift of x + n**, where n is the number of letters shifted. The result of the process is then taken under **modulo division,** essentially meaning that if a letter is shifted past the end of the alphabet, it wraps around to the beginning.