// get data from the html tags
var cx1 : HTMLInputElement = <HTMLInputElement>document.getElementById("x1");
var cy1 : HTMLInputElement = <HTMLInputElement>document.getElementById("y1");
var cx2 : HTMLInputElement = <HTMLInputElement>document.getElementById("x2");
var cy2 : HTMLInputElement = <HTMLInputElement>document.getElementById("y2");
var cx3 : HTMLInputElement = <HTMLInputElement>document.getElementById("x3");
var cy3 : HTMLInputElement = <HTMLInputElement>document.getElementById("y3");
var ux1 : HTMLInputElement = <HTMLInputElement>document.getElementById("x4");
var uy1 : HTMLInputElement = <HTMLInputElement>document.getElementById("y4");
var ans : HTMLInputElement = <HTMLInputElement>document.getElementById("ans");

// assign the variables to data got from html tags and in float data type
var x1 : number = parseFloat(cx1.value);
var y1 : number = parseFloat(cy1.value);
var x2 : number = parseFloat(cx2.value);
var y2 : number = parseFloat(cy2.value);
var x3 : number = parseFloat(cx3.value);
var y3 : number = parseFloat(cy3.value);
var ipx : number = parseFloat(ux1.value);
var ipy : number = parseFloat(uy1.value);

// calling function that return whether the point is inside or outside

function check()
{
    // using maths max function to get the highest cordinates of three vertex
    var xbig = Math.max(x1,x2,x3);      // got the highest x coordinate
    var ybig = Math.max(y1,y2,y3);      // got the highest y coordinate


    // checking if the given input less than or equal to highest coordinates
    // if it less then print point inside else print outside
    if(ipx >= xbig || ipy >= ybig)
    {
        document.getElementById("ans").innerHTML = "The point is inside the triangle";
    }
    else
    {
        document.getElementById("ans").innerHTML = "The point is outside the triangle";
    }

}
// i have written the code but it not working.