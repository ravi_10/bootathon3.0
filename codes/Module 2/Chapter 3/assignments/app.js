var cx1 = document.getElementById("x1");
var cy1 = document.getElementById("y1");
var cx2 = document.getElementById("x2");
var cy2 = document.getElementById("y2");
var cx3 = document.getElementById("x3");
var cy3 = document.getElementById("y3");
var ux1 = document.getElementById("x4");
var uy1 = document.getElementById("y4");
var ans = document.getElementById("ans");
var x1 = parseFloat(cx1.value);
var y1 = parseFloat(cy1.value);
var x2 = parseFloat(cx2.value);
var y2 = parseFloat(cy2.value);
var x3 = parseFloat(cx3.value);
var y3 = parseFloat(cy3.value);
var ipx = parseFloat(ux1.value);
var ipy = parseFloat(uy1.value);
function check() {
    var xbig = Math.max(x1, x2, x3);
    var ybig = Math.max(y1, y2, y3);
    if (ipx >= xbig || ipy >= ybig) {
        document.getElementById("ans").innerHTML = "The point is inside the triangle";
    }
    else {
        document.getElementById("ans").innerHTML = "The point is outside the triangle";
    }
}
//# sourceMappingURL=app.js.map